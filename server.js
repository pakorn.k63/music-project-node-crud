// start server with nodemon server.js in terminal
// test http://localhost:5333
var express = require('express')
var cors = require('cors')
var bodyParser = require('body-parser')
var jsonParser = bodyParser.json()
var jwt = require('jsonwebtoken')
//gen token with
const secretekey = 'pakorn-khamp-6300611'
const mysql = require('mysql2');
const PORT = process.env.PORT || 5333
const bcrypt = require('bcrypt');
const req = require('express/lib/request');
const saltRounds = 15;


const connection = mysql.createConnection({
  host: 'kutnpvrhom7lki7u.cbetxkdyhwsb.us-east-1.rds.amazonaws.com',
  port: '3306',
  user: 'ga3wzsxdd98wo52j',
  password: "u3rzhgst0419lkun",
  database: "ep6g1966rarvsxj0"
});

var app = express()
app.use(cors())
app.use(express.json())
/*---------------------------------------------------------------------------------------------------------------------------------*/
// for users table
  //checked
  app.get('/users', function (req, res, next) {
    connection.query(
      'SELECT * FROM `users`',
      function(err, results, fields) {
        res.json(results);
      }
    );
  })
  //checked
  app.get('/users/:username', function (req, res, next) {
    const username = req.params.username;
    connection.query(
      'SELECT * FROM `users` WHERE `username` = ?',
      [username],
      function(err, results) {
        res.json(results);
      }
    );
  })
  //checked
  app.put('/users', function (req, res, next) {
    bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
      connection.execute(
        'UPDATE `users` SET `nname`= ?, `fname`= ?, `lname`= ?, `password`= ?, `avatar`= ? WHERE username = ?',
        [req.body.nname, req.body.fname, req.body.lname, hash, req.body.avatar, req.body.username],
        function(err, results,fields) {
          if(err){res.json({status:'error',message:err});return}
          res.json({status:'profile changed'})
        }
      );
    });    
  })
  //checked
  app.delete('/users', function (req, res, next) {
    connection.query(
      'DELETE FROM `users` WHERE username = ?',
      [req.body.username],
      function(err, results) {
        res.json(results);
      }
    );
  })
  /*---------------------------------------------------------------------------------------------------------------------------------*/
  // for songs table
  //checked
  app.get('/songs', function (req, res, next) {
    connection.query(
      'SELECT * FROM `songs`',
      function(err, results, fields) {
        res.json(results);
      }
    );
  })
  //checked
  app.get('/songs/:sid', function (req, res, next) {
    const sid = req.params.sid;
    connection.query(
      'SELECT * FROM `songs` WHERE `sid` = ?',
      [sid],
      function(err, results) {
        res.json(results);
      }
    );
  })
  //checked
  app.get('/songs/username/:username', function (req, res, next) {
    const username = req.params.username;
    connection.query(
      'SELECT * FROM `songs` WHERE `username` = ?',
      [username],
      function(err, results) {
        res.json(results);
      }
    );
  })
  //checked
  app.post('/songs', jsonParser, function (req, res, next) {
    connection.execute(
      'INSERT INTO `songs`(`sname`, `username`, `lyric`, `stack`, `skey`, `sbpm`) VALUES (?, ?, ?, ?, ?, ?)',
      [req.body.sname, req.body.username, req.body.lyric, req.body.stack, req.body.skey, req.body.sbpm],
      function(err, results,fields) {
        if(err){res.json({status:'error',message:err});return}
        res.json({status:'song added'})
      }
    );
  })
  //checked
  app.put('/songs', function (req, res, next) {
    connection.execute(
      'UPDATE `songs` SET `sname`= ?, `lyric`= ?, `stack`= ?, `skey`= ?, `sbpm`= ? WHERE `sid` = ?',
      [req.body.sname, req.body.lyric, req.body.stack, req.body.skey, req.body.sbpm, req.body.sid],
      function(err, results,fields) {
        if(err){res.json({status:'error',message:err});return}
        res.json({status:'song updated'})
      }
    );
  })
  //checked
  app.delete('/songs', function (req, res, next) {
    connection.query(
      'DELETE FROM `songs` WHERE sid = ?',
      [req.body.sid],
      function(err, results) {
        res.json(results);
      }
    );
  })

  /*---------------------------------------------------------------------------------------------------------------------------------*/
  //checked
  app.post('/register', jsonParser, function(req, res, next){
    bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
      connection.execute(
        'INSERT INTO `users`(`username`, `nname`, `fname`, `lname`, `password`, `avatar`) VALUES (?, ?, ?, ?, ?, ?)',
        [req.body.username, req.body.nname, req.body.fname, req.body.lname, hash, req.body.avatar],
        function(err, results,fields) {
          if(err){res.json({status:'error',message:err});return}
          res.json({status:'registered'})
        }
      );
    });    
  })
  
  
  app.post('/login', jsonParser, function (req, res, next) {
    connection.execute(
      'SELECT * FROM `users` WHERE `username`=?',
      [req.body.username],
      function(err, users, fields) {
        if(err){res.json({status:'error',message:err});return}
        if(users.length==0){res.json({status:'error',message:'no user found'});return}
        bcrypt.compare(req.body.password, users[0].password, function(err, isLogin) {
          if(isLogin){
            var token = jwt.sign({ username: users[0].username }, secretekey, { expiresIn: '1h' });
            res.json({status:'ok[1]',message:'login success',token})
          }
          else {res.json({status:'error',message:'login failed'})}
        });
      }
    );
  })

  app.post('/authen', jsonParser, function (req, res, next) {
    try {
      const token = req.headers.authorization.split(' ')[1]
      var decoded = jwt.verify(token, secretekey);
      res.json({status:'ok',decoded})
    } catch (err) {
      res.json({status:'error',message:err.message})
    }
    
  })

  /*---------------------------------------------------------------------------------------------------------------------------------*/
  app.listen(PORT, jsonParser, function () {
    console.log('CORS-enabled web server listening on port' +PORT)
  })
    